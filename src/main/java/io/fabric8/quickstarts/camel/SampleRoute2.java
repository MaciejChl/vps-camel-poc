package io.fabric8.quickstarts.camel;


import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

@Component
public class SampleRoute2 extends RouteBuilder {


    @Override
    public void configure() throws Exception {

        restConfiguration().component("netty4-http").port(8088);

        rest("/check")
                .get("/1")
                .route()
               // .transform()
              //  .log("${body}")
                .to("netty4-http:http://google.pl?throwExceptionOnFailure=false");

    }





}
