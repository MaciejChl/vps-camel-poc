package io.fabric8.quickstarts.camel;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;


@Component
public class MyProcessor implements Processor {

    public void process(Exchange exchange) throws Exception {

        String myString = exchange.getIn().getBody(String.class);
        System.out.println("myString = " + myString);
      /*  String[] myArray = myString.split(System.getProperty("line.separator"));
        StringBuffer sb = new StringBuffer();
        for (String s : myArray) {
            sb.append(s).append(",");
        }
        System.out.println("MyProcessor complete");*/
        exchange.getIn().setBody(myString);
    }

    public MyProcessor() {
    }

}
