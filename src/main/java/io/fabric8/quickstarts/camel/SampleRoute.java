package io.fabric8.quickstarts.camel;


import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;




@Component
public class SampleRoute  extends RouteBuilder {


    @Autowired
    MyProcessor myProcessor;

    /*
    REST Components

    https://github.com/apache/camel/blob/master/camel-core/src/main/docs/rest-component.adoc

    camel servlet - only consumer

    restlet - different mvn repository, problems with building

    http4 - Cannot consume from http endpoint

    jetty - producer deprecated

    camel-netty4-http

    camel-undertow - ssl not supported

    */

    @Override
    public void configure() throws Exception {

        // works ok
        from("netty4-http:http://localhost:8080/test")
                .setHeader(Exchange.HTTP_METHOD, constant("POST"))
                .process(myProcessor)
                .log(body().toString())
                .to("netty4-http:http://dummy.restapiexample.com/api/v1/create");


/*
INPUT:
{
  "clientId": "1234",
  "clientName": "Acme"
}
TRANSFORMED:
{"customerId":"1234","name":"Acme"}
*/
        from("netty4-http:http://localhost:8080/transform")
                .process(myProcessor)
                .to("jolt:spec.json?inputType=JsonString&outputType=JsonString")
                .log(body().toString())
                .setHeader(Exchange.HTTP_METHOD, constant("POST"))
                .to("netty4-http:http://dummy.restapiexample.com/api/v1/create");


        // java.io.IOException: Istniejące połączenie zostało gwałtownie zamknięte przez zdalnego hosta
        from("netty4-http:http://localhost:8080/test1")
                .setHeader(Exchange.HTTP_METHOD, constant("POST"))
                .log("${body}")
                .to("netty4-http:https://fakerestapi.azurewebsites.net/api/Books");


        from("netty4-http:http://localhost:8080/test2")
                .setHeader(Exchange.HTTP_METHOD, constant("POST"))
                .log("${body}")
                //.to("netty4-http:https://jsonplaceholder.typicode.com/todos?ssl=true");
                // org.apache.camel.spring.boot.CamelSpringBootInitializationException: org.apache.camel.RuntimeCamelException: java.lang.NullPointerException

                //     .to("netty4-http:https://jsonplaceholder.typicode.com/todos?ssl=true&amp;throwExceptionOnFailure=false");
                // org.apache.camel.spring.boot.CamelSpringBootInitializationException: org.apache.camel.RuntimeCamelException: java.lang.NullPointerException

                .to("netty4-http:https://jsonplaceholder.typicode.com/todos?throwExceptionOnFailure=false");
        // <head><title>400 The plain HTTP request was sent to HTTPS port</title></head>

    }

}
